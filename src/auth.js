const API_URL = 'http://127.0.0.1:8000/api/v1/'
const LOGIN_URL = API_URL + 'log/'
const SIGNUP_URL = API_URL + 'reg/'

export default {
	user: {
		authenticated: false
	},
	error: false,
	login(creds) {

		return fetch(LOGIN_URL, {
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: "POST",
				body: JSON.stringify({
					"username": creds.user,
					"password": creds.pass
				})
			})
			.then((res) => res.json())
			.then((res) => {
				if (res == "User Does Not Exist") throw Error("User Does Not Exist")
				else return res
			})
			.then((res) => {
				console.log(res);

				localStorage.setItem('_first_name', res.first_name);
				localStorage.setItem('_last_name', res.last_name);
				localStorage.setItem('_token', res.token);
				localStorage.setItem('_user_name', res.username);
				localStorage.setItem('_email', res.email);
				localStorage.setItem('_is_staff', res.is_staff);
				this.user.authenticated = true;
				this.error = false;
				return res;
			}).catch((err) => {
				this.user.authenticated = false;
				this.error = true;
				throw Error("Login fialed");
			});
	},
	register(creds) {
		return fetch(SIGNUP_URL, {
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: "POST",
				body: JSON.stringify({
					"first_name": creds.fname,
					"last_name": creds.lname,
					"email": creds.email,
					"username": creds.user,
					"password": creds.pass,
					"is_staff": creds.is_staff
				})
			})
			.then((res) => res.json())
			.then((res) => {
				console.log(res);
				localStorage.setItem('_first_name', res.first_name);
				localStorage.setItem('_last_name', res.last_name);
				localStorage.setItem('_token', res.token);
				localStorage.setItem('_user_name', res.username);
				localStorage.setItem('_email', res.email);
				localStorage.setItem('_is_staff', res.is_staff);

				this.user.authenticated = true;
				this.error = false;
			}).catch((err) => {
				this.user.authenticated = false;
				this.error = true;
				throw Error("Registration failed");
			});
	},
	logout() {
		this.user.authenticated = false;
		localStorage.removeItem('_first_name');
		localStorage.removeItem('_last_name');
		localStorage.removeItem('_token');
		localStorage.removeItem('_user_name');
		localStorage.removeItem('_email');
		localStorage.removeItem('_is_staff');
	},
	checkAuth() {
		var jwt = localStorage.getItem('_token');
		//for preventing faking the jwt
		if (jwt !== undefined && jwt !== null && jwt !== "undefined" && jwt !== "") {
			this.user.authenticated = true;
			return true;
		} else {
			this.user.authenticated = false;
			return false;
		}
	},
	getAuthHeader() {
		return {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
			'Authorization': `token ${localStorage.getItem('_token')}`
		}
	},
	getUser() {
		return {
			'first_name': localStorage.getItem('_first_name'),
			'last_name': localStorage.getItem('_last_name'),
			'email': localStorage.getItem('_email'),
			'user_name': localStorage.getItem('_user_name'),
			'is_staff': localStorage.getItem('_is_staff')
		}
	}
}
