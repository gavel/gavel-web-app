const API_URL = 'https://api.judge0.com/'
const SUBMISSION_URL = API_URL + 'submissions/?base64_encoded=false&wait=true'

export default {
	compileCode(data) {
		return fetch(SUBMISSION_URL, {
				headers: {
					'Accept': 'application/json',
					'Content-Type': 'application/json'
				},
				method: "POST",
				body: JSON.stringify({
					"stdin": data.input,
					"source_code": data.code,
					"language_id": data.lang
				})
			}).then((res) => res.json())
			.catch((err) => {
				console.error(err);
			});
	}
}
