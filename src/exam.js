import auth from './auth'

const API_URL = 'http://127.0.0.1:8000/api/v1/'
const ANSWER_URL = API_URL + 'ans/'
const DEGREE_URL = API_URL + 'degree/'
const QUESTIONS_URL = API_URL + 'ques'
const EXAM_LIST = API_URL + 'list/'
const EXAM_URL = API_URL + 'exams/'

export default {
	addExam(id){
		return fetch(EXAM_LIST, {
			method: "POST",
			headers: auth.getAuthHeader(),
			body: JSON.stringify({
				"author": "44",
				"exam": id
			})
		}).then((res) => res.json())
		.catch((err) => {
			console.error(err);
		});
	},
	getExam(user){
		return fetch(`${EXAM_LIST}?username=${user}`, {
			method: "GET",
			headers: auth.getAuthHeader()
		}).then((res) => res.json())
		.catch((err) => {
			console.error(err);
		});
	},
	setExam(options){
		return fetch(EXAM_URL, {
			method: "POST",
			headers: auth.getAuthHeader(),
			body: JSON.stringify(options)
		}).then((res) => res.json())
		.catch((err) => {
			console.error(err);
		});
	},
	getQuestions(examId) {
		return fetch(`${QUESTIONS_URL}?id=${examId}`, {
				headers: auth.getAuthHeader()
			})
			.then((res) => res.json())
			.then((res)=> {console.log(res.choises); return res;})
			.catch((err) => {
				console.error(err);
			});
	},
	setQuestions(question) {
		return fetch(`${QUESTIONS_URL}/`, {
				method: "POST",
				headers: auth.getAuthHeader(),
				body: JSON.stringify(question)
			}).then((res) => res.json())
			.catch((err) => {
				console.error(err);
			});
	},
	setAnswers(answers) {
		return fetch(ANSWER_URL, {
				method: "POST",
				headers: auth.getAuthHeader(),
				body: JSON.stringify(answers)
			}).then((res) => res.json())
			.catch((err) => {
				console.error(err);
			});
	},
	getDegree(examId) {
		return fetch(DEGREE_URL, {
				method: "POST",
				headers: auth.getAuthHeader(),
				body: JSON.stringify(examId)
			}).then((res) => res.json())
			.catch((err) => {
				console.error(err);
			});
	}
}
