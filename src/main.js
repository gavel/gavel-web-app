// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueCodemirror from 'vue-codemirror'
import ToggleButton from 'vue-js-toggle-button'
import VueQrcodeReader from 'vue-qrcode-reader'
Vue.use(VueQrcodeReader)
Vue.use(ToggleButton)
// require styles
import 'codemirror/lib/codemirror.css'
import "vue-wysiwyg/dist/vueWysiwyg.css";

import wysiwyg from "vue-wysiwyg";
Vue.use(wysiwyg, {
	maxHeight: "500px",
	iconOverrides: {
		"bold": `<i class="material-icons">format_bold</i>`,
		"italic": `<i class="material-icons">format_italic</i>`,
		"underline": `<i class="material-icons">format_underlined</i>`,
		"justifyLeft": `<i class="material-icons">format_align_left</i>`,
		"justifyCenter": `<i class="material-icons">format_align_center</i>`,
		"justifyRight": `<i class="material-icons">format_align_right</i>`,
		"headings": `<i class="material-icons">text_fields</i>`,
		"link": `<i class="material-icons">insert_link</i>`,
		"code": `<i class="material-icons">code</i>`,
		"orderedList": `<i class="material-icons">format_list_numbered</i>`,
		"unorderedList": `<i class="material-icons">format_list_bulleted</i>`,
		"image": `<i class="material-icons">insert_photo</i>`,
		"table": `<i class="material-icons">border_all</i>`,
		"removeFormat": `<i class="material-icons">format_clear</i>`
	}
}); // config is optional. more below

// you can set default global options and events when use
Vue.use(VueCodemirror)

Vue.config.productionTip = false

import Datetime from 'vue-datetime'
// You need a specific loader for CSS files
import 'vue-datetime/dist/vue-datetime.css'

Vue.use(Datetime)

/* eslint-disable no-new */
new Vue({
	el: '#app',
	router,
	components: {
		App
	},
	template: '<App/>'
})
