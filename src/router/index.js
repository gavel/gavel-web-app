import Vue from 'vue'
import Router from 'vue-router'
import Login from '@/components/Login'
import Dashboard from '@/components/Dashboard'
import Register from '@/components/Register'
import ExamView from '@/components/ExamView'
import err404 from '@/components/404'
import err401 from '@/components/401'
import ExamResults from '@/components/ExamResults'
import ExamCreate from '@/components/ExamCreate'

Vue.use(Router)

export default new Router({
	routes: [{
			path: '/login',
			name: 'login',
			component: Login
		},
		{
			path: '/',
			name: 'login',
			component: Login
		}, {
			path: '/dashboard',
			name: 'dashboard',
			component: Dashboard
		},
		{
			path: '/ExamCreate',
			name: 'ExamCreate',
			component: ExamCreate
		}, {
			path: '/register',
			name: 'register',
			component: Register
		}, {
			path: '/examview',
			name: 'examview',
			component: ExamView
		}, {
			path: '/ExamResults',
			name: 'ExamResults',
			component: ExamResults
		}, , {
			path: '/404',
			name: '404',
			component: err404
		}, {
			path: '/401',
			name: '401',
			component: err401
		}
	]
})
