import Vue from 'vue'
import Btn from '../../../src/components/UI/Btn';

export { Btn };

Btn.install = function (Vue) {
	Vue.component(Btn.name, Btn);
}

export default Btn;
