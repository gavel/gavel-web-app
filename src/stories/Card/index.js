import Vue from 'vue'
import Card from '../../../src/components/UI/Card';

export { Card };

Card.install = function (Vue) {
	Vue.component(Card.name, Card);
}

export default Card;
