import Vue from 'vue'
import Login from '../../../src/components/UI/Login';

export { Login };

Login.install = function (Vue) {
	Vue.component(Login.name, Login);
}

export default Login;
