import Vue from 'vue'
import Register from '../../../src/components/UI/Register';

export { Register };

Register.install = function (Vue) {
	Vue.component(Register.name, Register);
}

export default Register;
