import Vue from 'vue'
import TextArea from '../../../src/components/UI/TextArea.vue';

export { TextArea };

TextArea.install = function (Vue) {
	Vue.component(TextArea.name, TextArea);
}

export default TextArea;
