import Vue from 'vue'
import TextField from '../../../src/components/UI/TextField';

export { TextField };

TextField.install = function (Vue) {
	Vue.component(TextField.name, TextField);
}

export default TextField;
