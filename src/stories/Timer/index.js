import Vue from 'vue'
import Timer from '../../../src/components/UI/Timer';

export { Timer };

Timer.install = function (Vue) {
	Vue.component(Timer.name, Timer);
}

export default Timer;
