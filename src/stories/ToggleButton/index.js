import Vue from 'vue'
import ToggleButton from '../../../src/components/UI/ToggleButton';

export { ToggleButton };

ToggleButton.install = function (Vue) {
	Vue.component(ToggleButton.name, ToggleButton);
}

export default ToggleButton;
