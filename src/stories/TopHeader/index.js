import Vue from 'vue'
import TopHeader from '../../../src/components/UI/TopHeader';

export { TopHeader };

TopHeader.install = function (Vue) {
	Vue.component(TopHeader.name, TopHeader);
}

export default TopHeader;
