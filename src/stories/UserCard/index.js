import Vue from 'vue'
import UserCard from '../../../src/components/UI/UserCard';

export { UserCard };

UserCard.install = function (Vue) {
	Vue.component(UserCard.name, UserCard);
}

export default UserCard;
