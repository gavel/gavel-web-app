import {
	storiesOf,
} from '@storybook/vue';
import {
	action
} from '@storybook/addon-actions';
import {
	linkTo
} from '@storybook/addon-links';

import Router from 'vue-router';

import TextArea from './TextArea';
import Welcome from './Welcome.vue';
import Card from './Card';
import Timer from './Timer';
import TextField from './TextField';
import Btn from './Btn'
import ToggleButton from './ToggleButton'
import TopHeader from './TopHeader'
import Login from './Login'
import Register from './Register'
import UserCard from './UserCard'

storiesOf('Welcome', module).add('to Storybook', () => ({
	components: {
		Welcome
	},
	template: '<welcome :showApp="action" />',
	methods: {
		action: linkTo('Button')
	},
}));

storiesOf('Card', module)
	.add('Basic Card', () => ({
		components: {
			Card
		},
		template: `
		<card style="max-width: 320px">
		 	<p>
				Lorem ipsum dolor sit amet consectetur adipisicing elit.
				Quaerat, sint hic repellendus aperiam vero consectetur,
				quam cum maxime officia cumque ipsa voluptates sit odit
				perferendis distinctio. Officiis, tempore. Dicta, doloribus?
		 	</p>
		</card>`
	}))
	.add('Header Card', () => ({
		components: {
			Card
		},
		template: `
		<card style="max-width: 320px">
			<header>
				<h1>Title</h1>
				<h2>Subtitle</h2>
			</header>
		 	<p>
				Lorem ipsum dolor sit amet consectetur adipisicing elit.
				Quaerat, sint hic repellendus aperiam vero consectetur,
				quam cum maxime officia cumque ipsa voluptates sit odit
				perferendis distinctio. Officiis, tempore. Dicta, doloribus?
		 	</p>
		</card>`
	}))
	.add('Cover Card', () => ({
		components: {
			Card
		},
		template: `
		<card cover="https://picsum.photos/800/450?image=392" style="max-width: 320px">
			<header>
				<h1>Title</h1>
				<h2>Subtitle</h2>
			</header>
		 	<p>
				Lorem ipsum dolor sit amet consectetur adipisicing elit.
				Quaerat, sint hic repellendus aperiam vero consectetur,
				quam cum maxime officia cumque ipsa voluptates sit odit
				perferendis distinctio. Officiis, tempore. Dicta, doloribus?
		 	</p>
		</card>`
	}))
	.add('Image Card', () => ({
		components: {
			Card
		},
		template: `<card cover="https://picsum.photos/800/450?image=392" style="max-width: 320px" />`
	}))
	.add('Image Card With Text', () => ({
		components: {
			Card
		},
		template: `<card  title="Title" subtitle="subtitle" cover="https://picsum.photos/800/450?image=392" style="max-width: 320px">
		</card>`
	}));

storiesOf('Timer', module).add('Timer', () => ({
	components: {
		Timer
	},
	template: `<timer/>`
}));

storiesOf('Text Field', module).add('Default text field', () => ({
		components: {
			TextField
		},
		template: `
		<form>
			<text-field style="max-width: 320px;" @input="action" type="text" label="Label"/>
			<text-field style="max-width: 320px;" @input="action" label="Placeholder" placeholder="Enter Your Name"/>
			<text-field style="max-width: 320px;" @input="action" label="shadow" shadow/>
			<text-field style="max-width: 320px;" @input="action" disabled type="text" label="disabled"/>
		</form>`,
		methods: {
			action: action('input')
		}
	}))
	.add('Dark text field', () => ({
		components: {
			TextField
		},
		template: `
		<form>
			<text-field style="max-width: 320px;" @input="action" type="text" label="Label" dark/>
			<text-field style="max-width: 320px;" @input="action" label="Placeholder" placeholder="Enter Your Name" dark/>
			<text-field style="max-width: 320px;" @input="action" label="shadow" dark shadow/>
			<text-field style="max-width: 320px;" @input="action" disabled dark type="text" label="disabled"/>
		</form>`,
		methods: {
			action: action('input')
		}
	}))
	.add('White text field', () => ({
		components: {
			TextField
		},
		template: `
		<form>
			<text-field style="max-width: 320px;" @input="action" type="text" label="Label" white/>
			<text-field style="max-width: 320px;" @input="action" label="Placeholder" placeholder="Enter Your Name" white/>
			<text-field style="max-width: 320px;" @input="action" label="shadow" white shadow/>
			<text-field style="max-width: 320px;" @input="action" white disabled type="text" label="disabled"/>
		</form>`,
		methods: {
			action: action('input')
		}
	}));

storiesOf('Button', module).add('Button', () => ({
	components: {
		Btn
	},
	template: `<div>
		<btn @click="action" style="max-width: 320px;"> Primary </btn>
		<br/>
		<btn @click="action" secondry style="max-width: 320px;"> Secondry </btn>
		<br/>
		<btn @click="action" tertiary style="max-width: 320px;"> tertiary </btn>
		<br/>
		<btn @click="action" dark style="max-width: 320px;"> dark  </btn>
		<br/>
		<btn @click="action" light style="max-width: 320px;"> White </btn>
		<br/>
		<btn @click="action" disabled style="max-width: 320px;"> Disabled </btn>
	</div>`,
	methods: {
		action: action('clicked')
	}
})).add('raised', () => ({
	components: {
		Btn
	},
	template: `
	<div>
		<btn @click="action" shadow style="max-width: 320px;"> Primary </btn>
		<br/>
		<btn @click="action" shadow secondry style="max-width: 320px;"> Secondry </btn>
		<br/>
		<btn @click="action" shadow tertiary style="max-width: 320px;"> tertiary </btn>
		<br/>
		<btn @click="action" shadow dark style="max-width: 320px;"> dark  </btn>
		<br/>
		<btn @click="action" shadow light style="max-width: 320px;"> White </btn>
		<br/>
		<btn @click="action" shadow disabled style="max-width: 320px;"> Disabled </btn>
	</div>`,
	methods: {
		action: action('clicked')
	}
}));

storiesOf('Anchors', module).add('anchor link', () => ({
	template: `<a href="#"> Hello World! </a>`
}));


storiesOf('Toggle Button', module).add('toggles', () => ({
	components: {
		ToggleButton
	},
	template: `
	<div>
	Copyright (c) 2017 Yev Vlasenko
	<br/> <br/>
	<toggle-button></toggle-button>
	<br/>
	<toggle-button :labels="true" color="#82C7EB"/>
	<br/>
	<toggle-button :value="true"
          name="phone"
          :labels="{checked: 'Android', unchecked: 'iPhone'}"
          :color="{checked: '#7DCE94', unchecked: '#82C7EB'}"
          :width="100"/>
		  <br/>

        <toggle-button
          :width="150"
          :value="true"
          color="#BB99CD"
          :labels="{checked: 'Only left label!', unchecked: ''}"/>
			<br/>
        <toggle-button
          :value="false"
          :width="150"
          color="#BB99CD"
          :labels="{checked: '', unchecked: 'Only right label!'}"/>
		  <br/>
        <toggle-button
          :value="false"
          :width="100"
          :labels="{unchecked: 'Disabled'}"
          :disabled="true"/>
		  <br/>
        <toggle-button
          :value="false"
          :width="200"
          :color="{disabled: '#FF6699'}"
          :labels="{unchecked: 'Custom disabled color'}"
          :disabled="true"/>
		  <br/>
        <toggle-button :value="true"
          :labels="{checked: 'Button', unchecked: 'Collor'}"
          :color="{checked: '#7DCE94', unchecked: '#82C7EB'}"
          :width="100"
		  :switchColor="{checked: 'linear-gradient(red, yellow)', unchecked: '#F2C00B'}"/>

			<br/>
			<toggle-button
			:color="{unchecked: '#7ac2d0'}"
			:width=148
			:height=40
			:labels="{checked: 'staff', unchecked: 'member'}"
			:sync="true"/>
	</div>`
}));

storiesOf('Images', module).add('responsive', () => ({
	template: `
	<div>
	<div class="row">
	<picture class="col-6">
			<img src="https://picsum.photos/1600/800" class="img-responsive" >
		</picture>
		<picture class="col-6">
			<img src="https://picsum.photos/1200/600" class="img-responsive" >
		</picture>
	</div>
	<br/>
	<div class="row">
	<picture class="col-6">
			<img src="https://picsum.photos/1000/500" class="img-responsive" >
		</picture>
		<picture class="col-6">
			<img src="https://picsum.photos/1400/700" class="img-responsive" >
		</picture>
	</div>
	</div>`
})).add('rounded', () => ({
	template: `
	<div>
	<div class="row">
	<picture class="col-6">
			<img src="https://picsum.photos/1600/800" class="img-responsive img-rounded" >
		</picture>
		<picture class="col-6">
			<img src="https://picsum.photos/1200/600" class="img-responsive img-rounded" >
		</picture>
	</div>
	<br/>
	<div class="row">
	<picture class="col-6">
			<img src="https://picsum.photos/1000/500" class="img-responsive img-rounded" >
		</picture>
		<picture class="col-6">
			<img src="https://picsum.photos/1400/700" class="img-responsive img-rounded" >
		</picture>
	</div>
	</div>`
}))
.add('avatar', () => ({
	template: `
	<div class="row>
		<picture class="col-6">
			<img src="https://picsum.photos/600" class="img-responsive img-avatar" alt="">
		</picture>
	</div>`
}));

storiesOf('Headings', module).add('headings', () => ({
	template: `
	<div>
		<h1> Heading 1 </h1>
		<h2> Heading 2 </h2>
		<h3> Heading 3 </h3>
		<h4> Heading 4 </h4>
		<h5> Heading 5 </h5>
		<h6> Heading 6 </h6>
	</div>`
}));

storiesOf('Paraghraphs', module).add('p', () => ({
	template: `<div>
	<p>
		Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent malesuada mauris eu condimentum scelerisque. Pellentesque porta sodales pharetra. Vivamus vulputate facilisis lorem, sed auctor lacus. Cras diam odio, fringilla commodo sem vel, luctus gravida risus. Quisque eu lectus odio. Quisque sollicitudin purus et augue faucibus, sed blandit nibh sollicitudin. In scelerisque nunc id arcu egestas ullamcorper.
	</p>
	<p>
	Praesent blandit nec justo ac commodo. Cras fermentum magna vel faucibus feugiat. Vivamus at malesuada eros, sed feugiat mi. Cras egestas sodales urna, sed commodo nibh ultricies id. Nullam eu congue dolor. Cras ac consequat justo, sed laoreet est. Integer sit amet volutpat nisl, sit amet pellentesque elit.
	</p>
	</div>
	`
}));


storiesOf('Tables', module).add('table', () => ({
	template: `
	<div>
	<table>
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Questions</th>
									<th scope="col">Score</th>
									<th scope="col">Total</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scope="row">0</th>
									<td>Lorem</td>
									<td class="score">66</td>
									<td>66</td>
								</tr>
								<tr>
									<th scope="row">1</th>
									<td>Ipsum</td>
									<td class="score">77</td>
									<td>77</td>
								</tr>
								<tr>
									<th scope="row">2</th>
									<td>Dolor</td>
									<td class="score">99</td>
									<td>99</td>
								</tr>
								<tr v-if="no_records==true">
    							<td colspan="4" align="center"> There is no records</td>
							</tr>
							</tbody>
	</table>
	<table class="table-dark">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Questions</th>
									<th scope="col">Score</th>
									<th scope="col">Total</th>
								</tr>
							</thead>
							<tbody>
							<tr>
							<th scope="row">0</th>
							<td>Lorem</td>
							<td class="score">66</td>
							<td>66</td>
						</tr>
						<tr>
							<th scope="row">1</th>
							<td>Ipsum</td>
							<td class="score">77</td>
							<td>77</td>
						</tr>
						<tr>
							<th scope="row">2</th>
							<td>Dolor</td>
							<td class="score">99</td>
							<td>99</td>
						</tr>
						<tr v-if="no_records==true">
    							<td colspan="4" align="center"> There is no records</td>
							</tr>
							</tbody>
	</table>
	</div>

	`,
	data() {
		return {
			no_records: false
		}
	}
})).add('table stripe', () => ({
	template: `
	<div>
	<table class="stripe">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Questions</th>
									<th scope="col">Score</th>
									<th scope="col">Total</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scope="row">0</th>
									<td>Lorem</td>
									<td class="score">66</td>
									<td>66</td>
								</tr>
								<tr>
									<th scope="row">1</th>
									<td>Ipsum</td>
									<td class="score">77</td>
									<td>77</td>
								</tr>
								<tr>
									<th scope="row">2</th>
									<td>Dolor</td>
									<td class="score">99</td>
									<td>99</td>
								</tr>
								<tr v-if="no_records==true">
    							<td colspan="4" align="center"> There is no records</td>
							</tr>
							</tbody>
	</table>
	<table class="table-dark stripe">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Questions</th>
									<th scope="col">Score</th>
									<th scope="col">Total</th>
								</tr>
							</thead>
							<tbody>
							<tr>
							<th scope="row">0</th>
							<td>Lorem</td>
							<td class="score">66</td>
							<td>66</td>
						</tr>
						<tr>
							<th scope="row">1</th>
							<td>Ipsum</td>
							<td class="score">77</td>
							<td>77</td>
						</tr>
						<tr>
							<th scope="row">2</th>
							<td>Dolor</td>
							<td class="score">99</td>
							<td>99</td>
						</tr>
						<tr v-if="no_records==true">
    							<td colspan="4" align="center"> There is no records</td>
							</tr>
							</tbody>
	</table>
	</div>

	`,
	data() {
		return {
			no_records: false
		}
	}
})).add('table shadow', () => ({
	template: `
	<div>
	<table class="table-shadow ">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Questions</th>
									<th scope="col">Score</th>
									<th scope="col">Total</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<th scope="row">0</th>
									<td>Lorem</td>
									<td class="score">66</td>
									<td>66</td>
								</tr>
								<tr>
									<th scope="row">1</th>
									<td>Ipsum</td>
									<td class="score">77</td>
									<td>77</td>
								</tr>
								<tr>
									<th scope="row">2</th>
									<td>Dolor</td>
									<td class="score">99</td>
									<td>99</td>
								</tr>
								<tr v-if="no_records==true">
    							<td colspan="4" align="center"> There is no records</td>
							</tr>
							</tbody>
	</table>
	<table class="table-dark table-shadow">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Questions</th>
									<th scope="col">Score</th>
									<th scope="col">Total</th>
								</tr>
							</thead>
							<tbody>
							<tr>
							<th scope="row">0</th>
							<td>Lorem</td>
							<td class="score">66</td>
							<td>66</td>
						</tr>
						<tr>
							<th scope="row">1</th>
							<td>Ipsum</td>
							<td class="score">77</td>
							<td>77</td>
						</tr>
						<tr>
							<th scope="row">2</th>
							<td>Dolor</td>
							<td class="score">99</td>
							<td>99</td>
						</tr>
						<tr v-if="no_records==true">
    							<td colspan="4" align="center"> There is no records</td>
							</tr>
							</tbody>
	</table>
	</div>

	`,
	data() {
		return {
			no_records: false
		}
	}
})).add('empty', () => ({
	template: `
	<div>
	<table class="stripe">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Questions</th>
									<th scope="col">Score</th>
									<th scope="col">Total</th>
								</tr>
							</thead>
							<tbody>
							<tr v-if="no_records==true">
    							<td colspan="4" align="center"> There is no records</td>
							</tr>
							</tbody>
	</table>
	<table class="table-dark stripe">
							<thead>
								<tr>
									<th scope="col">#</th>
									<th scope="col">Questions</th>
									<th scope="col">Score</th>
									<th scope="col">Total</th>
								</tr>
							</thead>
							<tbody>
							<tr v-if="no_records==true">
    							<td colspan="4" align="center"> There is no records</td>
							</tr>
							</tbody>
	</table>
	</div>

	`,
	data() {
		return {
			no_records: true
		}
	}
}));




storiesOf('Header', module).add('header', () => ({
	components: {
		Router,
		TopHeader,
		Timer
	},
	template: `
	<div>
		<top-header @icon-click="action"/>
		<br/>
		<top-header icon="menu" title="Title" @icon-click="action"/>
		<br/>
		<top-header icon="arrow_back" title="Title" dark @icon-click="action"/>
		<br/>
		<top-header icon="arrow_back" title="Title" bgcolor="#263238" centertitle titlecolor="#fff" @icon-click="action"/>
		<br/>
		<top-header icon="menu" title="Title" centertitle @icon-click="action"> <timer style="float:right"/> </top-header>
		<br/>
	</div>`,
	methods: {
		action: action('icon-click')
	}
}));


storiesOf('Text Area', module).add('textarea', () => ({
		components: {
			TextArea
		},
		template: `
	<div>
	default
		<text-area @input="action" placeholder="Enter text..."/>
	dark
		<text-area @input="action" dark placeholder="Enter text..."/>
	</div>`,
		methods: {
			action: action('input')
		}
	}))
	.add('disabled textarea', () => ({
		components: {
			TextArea
		},
		template: `
	<div>
	disabled
		<text-area @input="action" disabled placeholder="Enter text..."/>
	disabled dark
		<text-area @input="action" dark disabled placeholder="Enter text..."/>
	</div>`,
		methods: {
			action: action('input')
		}
	}))
	.add('hand-writing textarea', () => ({
		components: {
			TextArea
		},
		template: `
	<div>
	Copyright (c) Paper Textarea A PEN BY Marc Malignan
	<br/>
	<br/>
		<text-area paper value="Hello there \nHere's a paper text area, feel free to type something ..." @input="action"/>
	</div>`,
		methods: {
			action: action('input')
		}
	}));


storiesOf('Forms', module).add('log in', () => ({
		components: {
			Router,
			Login
		},
		template: `<login @submit="action"/>`,
		methods: {
			action: action('submit')
		},

	}))
	.add('register', () => ({
		components: {
			Router,
			TextField,
			Btn,
			Card,
			ToggleButton,
			Register
		},
		template: `<register @submit="action"/>`,
		methods: {
			action: action('submit')
		},
		data() {
			return {
				creds: {
					fname: "",
					lname: "",
					user: "",
					email: "",
					pass: "",
					is_staff: false
				}
			}
		}
	}));

	storiesOf('User', module).add('card', () => ({
		components: {
			UserCard
		},
		template: `<user-card @logout="action"/>`,
		methods: {
			action: action('logout')
		}
	}))
